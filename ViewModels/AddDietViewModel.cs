﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using DietManagerStudio.Converters;
using DietManagerStudio.Models;
using DietManagerStudio.PageSwitcherTool;
using DietManagerStudio.RegistryMecanisme;
using DietManagerStudio.ViewModels.ProductViewModels;
using DietManagerStudio.Views.Layouts;
using FoodIntakesManagerStudio.Models;

namespace DietManagerStudio.ViewModels.Diet
{
	public class AddDietViewModel:ViewModelBase
	{
		public AddDietViewModel()
		{
            //this Get a value from contain her layout
			Registry<AddDietViewModel, ObservableCollection<FoodProduct>>.Subscribe((o, e) =>
			{
				if (e.Mode == ActionMode.Update)
				{
					foreach (var foodIntakeProductList in FoodIntakeProductList)
					{
						if (foodIntakeProductList.FoodIntakes.Id == _selectedFoodIntake.Id)
						{
							foodIntakeProductList.ProductList = e.Value;
						}
					}
				}
			}, this.GetHashCode().ToString());

            Registry<AddDietViewModel, ObservableCollection<FoodIntakeProductList>>.Subscribe((o, e) =>
		    {
		        if (e.Mode == ActionMode.Update)
		        {
		            FoodIntakeProductList = e?.Value;
		        }
		    }, "");

			Registry<AddDietViewModel, ObservableCollection<FoodIntakeProductList>>.Subscribe((o, e) =>
	  {
		  if (e.Mode == ActionMode.Update)
		  {
			  FoodIntakeProductList = e?.Value;
		  }
	  }, "");


			if (!DesignerProperties.GetIsInDesignMode(new DependencyObject()))
			{
				FoodIntakes = CollectionsConverter.ToObservableCollection(FoodIntakesModel.GetFoodIntakes().ToList());
				Products = CollectionsConverter.ToObservableCollection(FoodProductModel.GetFoodProduct());
				FoodIntakeProductList = new ObservableCollection<FoodIntakeProductList>();
				Diet = new Models.Diet();
			}
			else
			{
				FoodIntakes = new ObservableCollection<FoodIntake>
				{
					new FoodIntake
					{
						Name = "Завтра",
						Number = 1
					},
					new FoodIntake
					{
						Name = "Обед",
						Number = 2
					},
					new FoodIntake
					{
						Name = "Ужин",
						Number = 3
					}
				};

				Products = new ObservableCollection<FoodProduct>
				{
					new FoodProduct(
						"Рис", 12, 3,33, 100, new Measuring { Name = "Грамм" }, new Models.Category { Name = "Крупы" }
						),
					new FoodProduct(
						"Гречка", 12, 3,33, 100, new Measuring { Name = "Грамм" }, new Models.Category { Name = "Крупы" }
						),
					new FoodProduct(
						"Молоко", 12, 3,33, 100, new Measuring { Name = "Литр" }, new Models.Category { Name = "Молочные продукты" }
						)
				};

				FoodIntakeProductList = new ObservableCollection<FoodIntakeProductList>
				{
					new FoodIntakeProductList(FoodIntakes.ElementAt(0), Products),
					new FoodIntakeProductList(FoodIntakes.ElementAt(1), Products),
					new FoodIntakeProductList(FoodIntakes.ElementAt(1), new ObservableCollection<FoodProduct>
					{
					new FoodProduct(
						"Пшонка", 12, 3,33, 100, new Measuring { Name = "Грамм" }, new Models.Category { Name = "Крупы" }
						),
					})
				};
			}
		}

		private string _name;
		public string Name
		{
			get
			{
				return _name;
			}
			set
			{
				_name = value;
				OnPropertyChanged("Name");
			}
		}

		private double _weight;
		public double Weight
		{
			get
			{
				return _weight;
			}
			set
			{
				_weight = value;
				OnPropertyChanged("Weight");
			}
		}

        private Client _selectedClient;
        public Client SelectedClientForDiet
        {
            get
            {
                return _selectedClient;
            }
            set
            {
                _selectedClient = value;
                OnPropertyChanged("SelectedClient");
            }
        }
        

        private Models.Diet _diet;
		public Models.Diet Diet
		{
			get
			{
				return _diet;
			}
			set
			{
				_diet = value;
				OnPropertyChanged("Diet");
			}
		}

		private FoodIntake _foodIntake;
		public FoodIntake SelectedFoodIntake
		{
			get
			{
				return _foodIntake;
			}
			set
				{
				_foodIntake = value;
					OnPropertyChanged("SelectedFoodIntake");=
			}
		}

		private ObservableCollection<FoodIntakeProductList> _foodIntakeProductList;
		public ObservableCollection<FoodIntakeProductList> FoodIntakeProductList
		{
			get
			{
				return _foodIntakeProductList;
			}
			set
			{
				_foodIntakeProductList = value;
				OnPropertyChanged("FoodIntakeProductList");
			}
		}

		private DietComponent _selectedDietComponent;
		public DietComponent SelectedDietComponent
		{
			get
			{
				return _selectedDietComponent;
			}
			set
			{
				_selectedDietComponent = value;
				OnPropertyChanged("SelectedDietComponent");
			}
		}

		private ObservableCollection<FoodIntake> _foodIntakes;
		public ObservableCollection<FoodIntake> FoodIntakes
		{
			get
			{
				return _foodIntakes;
			}
			set
			{
				_foodIntakes = value;
				OnPropertyChanged("FoodIntak");
			}
		}

		private ObservableCollection<FoodProduct> _productList;
		public ObservableCollection<FoodProduct> Products
		{
			get
			{
				return _productList;
			}
			set
			{
				_productList = value;
				OnPropertyChanged("Products");
			}
		}

		private FoodProduct _selectedProduct;
		public FoodProduct SelectedProduct
		{
			get
			{
				return _selectedProduct;
			}
			set
			{
				_selectedProduct = value;
				OnPropertyChanged("SelectedProduct");
			}
		}

		private RelayCommand _addDiet;
		public ICommand AddDiet
		{
			get
			{
				return new RelayCommand(
					(o) =>
					{
						List<DietComponent> dietComponent = new List<DietComponent>();
						foreach (FoodIntakeProductList foodIntakeProducts in FoodIntakeProductList)
						{
                            if(foodIntakeProducts.ProductList!=null)
							foreach (FoodProduct product in foodIntakeProducts.ProductList)
							{
								dietComponent.Add(new DietComponent(foodIntakeProducts.FoodIntakes, product, product.Weight));
							}
						}

						var newDiet = new Models.Diet(Name, dietComponent);
						DietModel.CreateDiet(newDiet);
						Registry<DietListViewModel, Models.Diet>.GenerateEvent(this, new RegistryEventArgs<Models.Diet>(newDiet, ActionMode.Update), null);
					}
					);
			}
		}

		private FoodIntake _selectedFoodIntake = null;
		private RelayCommand _addProduct;
		public ICommand AddProduct
		{
			get
			{
				return new RelayCommand(
					(o) =>
					{
						FoodIntake selectedFoodIntake = (FoodIntake) o;
						_selectedFoodIntake = selectedFoodIntake;
						FoodIntakeProductList selectedFoodIntakes = FoodIntakeProductList.FirstOrDefault(el => el.FoodIntakes.Id == (int)selectedFoodIntake.Id);

							ObservableCollection<FoodProduct> productsFromSelectedFoodIntake = selectedFoodIntakes?.ProductList;

                        Registry<SelectingProductsViewModel, ObservableCollection<FoodProduct>>
							.GenerateEvent(this, new RegistryEventArgs<ObservableCollection<FoodProduct>>(productsFromSelectedFoodIntake, ActionMode.Update), null);

						Registry<ProductsChooseLayoutViewModel, int>.PublicValue(this.GetHashCode(), AppLayouts.ProductsChooseLayout.GetHashCode().ToString());

						Registry<MainLayoutViewModel, UserControl>
						.GenerateEvent(this, new RegistryEventArgs<UserControl>(AppLayouts.ProductsChooseLayout, ActionMode.Update), null);
					}
					);
			}
		}
	}
}
